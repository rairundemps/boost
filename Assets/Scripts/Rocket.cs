﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

[DisallowMultipleComponent]
[RequireComponent(typeof(Rigidbody))]
public class Rocket : MonoBehaviour
{
    [SerializeField] private float _rocketShipThrustPower = 100f;
    [SerializeField] private float _rocketShipRotationSpeed = 100f;

    private Rigidbody _rigidbody;

    private bool _areCollisionsEnabled = true;
    private bool _isTransitioning = false;

    public event Action Die;
    public event Action LevelFinished;
    public event Action ApplyingThrust;
    public event Action StopApplyingThrust;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (!_isTransitioning)
        {
            RespondToThrustInput();
            RespondToRotationInput();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!_areCollisionsEnabled || _isTransitioning)
            return;

        // todo compareTo
        switch (collision.gameObject.tag)
        {
            case "Friendly":
                break;
            case "Finish":
                StartSequence(LevelFinished);
                break;
            default:
                StartSequence(Die);
                break;
        }
    }

    private void StartSequence(Action action)
    {
        _isTransitioning = true;
        action?.Invoke();
    }

    private void RespondToRotationInput()
    {
        // take manual control of rotation
        _rigidbody.angularVelocity = Vector3.zero;

        float rotation = _rocketShipRotationSpeed * Time.deltaTime;

        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(Vector3.forward * rotation);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(-Vector3.forward * rotation);
        }
    }

    private void RespondToThrustInput()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            ApplyThrust();
        }
        else
        {
            StopApplyingThrust?.Invoke();
        }
    }

    private void ApplyThrust()
    {
        _rigidbody.AddRelativeForce(Vector3.up * _rocketShipThrustPower *
                                                               Time.deltaTime);
        ApplyingThrust?.Invoke();
    }
}
