using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    [SerializeField] private Rocket _rocket;

    private const float _loadDelayTime = 1f;

    private void OnEnable()
    {
        _rocket.Die += LoadFirstLevel;
        _rocket.LevelFinished += LoadNextLevel;
    }

    private void OnDisable()
    {
        _rocket.Die -= LoadFirstLevel;
        _rocket.LevelFinished -= LoadNextLevel;
    }

    private void LoadFirstLevel()
    {
        StartCoroutine(LoadLevel(0));
    }

    private void LoadNextLevel()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int nextSceneIndex = currentSceneIndex + 1;

        if (SceneManager.sceneCountInBuildSettings == nextSceneIndex)
            nextSceneIndex = 0;

        StartCoroutine(LoadLevel(nextSceneIndex));
    }

    private IEnumerator LoadLevel(int number)
    {
        yield return new WaitForSeconds(_loadDelayTime);
        SceneManager.LoadScene(number);
    }
}
