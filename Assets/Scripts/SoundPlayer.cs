using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(AudioSource))]
public class SoundPlayer : MonoBehaviour
{
    [SerializeField] private AudioClip _engine;
    [SerializeField] private AudioClip _death;
    [SerializeField] private AudioClip _success;
    [SerializeField] private Rocket _rocket;

    private AudioSource _audioSource;

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        _rocket.Die += PlayDeathSound;
        _rocket.LevelFinished += PlayLevelFinishedSound;
        _rocket.ApplyingThrust += PlayEngineSound;
        _rocket.StopApplyingThrust += StopEngineSound;
    }

    private void OnDisable()
    {
        _rocket.Die -= PlayDeathSound;
        _rocket.LevelFinished -= PlayLevelFinishedSound;
        _rocket.ApplyingThrust -= PlayEngineSound;
        _rocket.StopApplyingThrust -= StopEngineSound;
    }

    private void PlayDeathSound()
    {
        _audioSource.Stop();
        _audioSource.PlayOneShot(_death);
    }

    private void PlayLevelFinishedSound()
    {
        _audioSource.Stop();
        _audioSource.PlayOneShot(_success);
    }

    private void PlayEngineSound()
    {
        if (!_audioSource.isPlaying)
            _audioSource.PlayOneShot(_engine);
    }

    private void StopEngineSound()
    {
        if (_audioSource.isPlaying)
            _audioSource.Stop();
    }
}
