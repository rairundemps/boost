using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleEmitter : MonoBehaviour
{
    [SerializeField] private ParticleSystem _engine;
    [SerializeField] private ParticleSystem _death;
    [SerializeField] private ParticleSystem _success;
    [SerializeField] private Rocket _rocket;

    private void OnEnable()
    {
        _rocket.Die += EmitDeathParticles;
        _rocket.LevelFinished += EmitLevelFinishedParticles;
        _rocket.ApplyingThrust += EmitEngineParticles;
        _rocket.StopApplyingThrust += StopEngineParticles;
    }

    private void OnDisable()
    {
        _rocket.Die -= EmitDeathParticles;
        _rocket.LevelFinished -= EmitLevelFinishedParticles;
        _rocket.ApplyingThrust -= EmitEngineParticles;
        _rocket.StopApplyingThrust -= StopEngineParticles;
    }

    private void EmitDeathParticles()
    {
        _engine.Stop();
        _death.Play();
    }

    private void EmitLevelFinishedParticles()
    {
        _engine.Stop();
        _success.Play();
    }

    private void EmitEngineParticles()
    {
        if (!_engine.isPlaying)
            _engine.Play();
    }

    private void StopEngineParticles()
    {
        if (_engine.isPlaying)
            _engine.Stop();
    }

}
