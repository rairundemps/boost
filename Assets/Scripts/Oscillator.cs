﻿using UnityEngine;

[DisallowMultipleComponent]
public class Oscillator : MonoBehaviour
{
    [SerializeField] private Vector3 _movement = new Vector3(37.5f, 0f, 0f);
    [SerializeField] private float _period = 5f;

    private Vector3 _startingPosition;
    private const float _tau = Mathf.PI * 2f;

    private void Start()
    {
        _startingPosition = transform.position;
    }

    private void Update()
    {
        if (_period <= Mathf.Epsilon)
        {
            return;
        }

        float cycles = Time.time / _period;
        float rawSinInfo = Mathf.Sin(cycles * _tau);
        Vector3 offset = (rawSinInfo / 2f + 0.5f) * _movement;

        transform.position = _startingPosition + offset;
    }
}
